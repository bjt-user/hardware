#### ratings

|name|brand|accessibility rating|noise blocking rating|overall rating|comment|
|---|---|---|---|---|---|
| Awaita Booksize | Awaita | 7/10 | 4/10 | 7/10 | good home server case (downside: power led is very bright) |
| Dell Optiplex | Dell | 0/10 | 3/10 | 0/10 | just a pain to open and put back together (weird mechanism) |
| Lenovo Thinkcentre | Lenovo | 2/10 | 5/10 | 2/10 | only suited for the motherboard that comes with it |
| Cooler Master MasterBox Q300L | CoolerMaster | 7/10 | 4/10 | 8/10 | easily accessible, has holes to mount stuff like a handle |

#### home server

Search for `Awaita Booksize` or `Chenbro`.\
Might also try 19'' case at some point.

You might also search for `HTPC` (which stands probably for home theatre pc, but they are lying down).

Or search for `flat computer case`.

#### Lenovo Thinkcenter

The case of an old Lenovo Thinkcenter desktop pc is good.\
Upsides:
- there is a handle at the top to carry the case
- easy to open

Downsides:
- the case fan is 90mm and I had to drill additional holes
- mainboard is hard to take out (you need to remove the psu, hard drive, etc)

#### Cooler Master MasterBox Q300L

Dimensions: H38cm x W22cm x D37.5cm

It is pretty wide with 22cm.\
=> check if that is really necessary

A handle can be built on the top because of the holes in the case.

Test how good and fast components can be placed into it.

A speaker could be screwed in the case due to the holes.

The front panel USB and audio connectors dont seem to fit into an older Lenovo Thinkcentre motherboard.

With a Lenovo ThinkCentre MOBO it does not seem possible. PSU cable is too short. Some connectors dont fit anywhere.

#### resolution names

full hd = 1080p

hd = 720p

#### authentication via id

720p is not enough when trying to authenticate with an identity card.

#### webcams

| name | manufacturer | resolution | personal rating |
|-|-|-|-|
| HD 720p webcam TRINO | Trust | 720p | 2/10 |
| C270 HD WEBCAM | Logitech | 720p | 2/10 |

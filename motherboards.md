#### form factor

micro ATX should fit into an ATX case, but the screw holes at the bottom will not be used.

#### ratings

| motherboard | rating | comment |
|---|---|---|
| dell optiplex motherboard (from prebuilt pc) | 0/10 | (doesn't boot anymore after 2 years, is very loud, immediatelly turns on when connected to power, always starts up "dell supportassistent" |
| ASRock D1800M SoC So.BGA Dual Channel DDR3 mATX | 6/10 | fanless, good for a server, rtc option is available (scheduled boot) |
| ASRock H470M-HDV | 6/10 | intel cpu fan makes some noise, but I will change it |
| Lenovo ThinkCentre (from prebuilt pc) | 0/10 | can't even find the pin headers for the power button (no labels) |

#### motherboard info from os

Get info about the motherboard from your operating system:
```
cat /sys/devices/virtual/dmi/id/board_{vendor,name,version}
```

## bios
#### rtc in ASRock

The ASRock motherboard has an rtc bios option where you can make the computer start up at a specific time every day.\
=> but by default the BIOS clock will be in UTC.\
And you should not change the BIOS time, because the operating system will change it anyway.

You should set the RTC set on alarm or however it is called to `let the operating system handle it`

=> booting the computer at a specific time can be done better in Linux

## specifics about boards

#### ASRock H470M-HDV

Does boot without having a cpu fan connected!\
This is a plus point.\
So you can use passive cooling.

## troubleshooting

#### ASRock board does not detect SSD anymore

After changing cpu fans the motherboard doesnt detect the SSD anymore.\
Unclear what happened here.

It shows up in boot priority only if `CSM` (compatibility support module) is enabled.\
But it cannot boot into the SSD because there it is installed with EFI...\
And it worked right after the installation...but not anymore...

Reinstalled Arch Linux the same way as before and it worked again.\
No idea what happened here.

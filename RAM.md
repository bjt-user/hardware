## form factor

https://en.wikipedia.org/wiki/DIMM

A DIMM, commonly called a RAM stick, comprises a series of dynamic random-access memory integrated circuits. These modules are mounted on a printed circuit board and designed for use in personal computers, workstations, printers, and servers. They are the predominant method for adding memory into a computer system. The vast majority of DIMMs are standardized through JEDEC standards, although there are proprietary DIMMs. DIMMs come in a variety of speeds and sizes, but generally are one of two lengths - PC which are 133.35 mm (5.25 in) and **laptop (SO-DIMM)** which are about half the size at 67.60 mm (2.66 in).

For laptops you need the SO-DIMM type of RAM.



show your RAM speed (i.e. 1600 MT/s), form factor and type (DDR3, DDR4, DDR5, etc...) on each slot:
```
sudo dmidecode --type memory | less
```

#### gap

the gap of the ram is not always at the same spot.

DDR4 RAM did not fit into a motherboard.\
Probably because the motherboard is only for DDR3-RAM.

#### ddr4

https://en.wikipedia.org/wiki/DDR4_SDRAM

-    DDR4-1600 (PC4-12800)
-    DDR4-1866 (PC4-14900)
-    DDR4-2133 (PC4-17000)
-    DDR4-2400 (PC4-19200)
-    DDR4-2666 (PC4-21333)
-    DDR4-2933 (PC4-23466)
-    DDR4-3200 (PC4-25600)

On my RAM module was a sticker that said "PC4 - 2666V", which indicates that it is DDR4 RAM with 2666 Mhz.\
Googling the serial number confirmed this.

***
#### buying RAM

Before buying RAM you should look into the datasheet to find a Qualified Vendors List, to see if it will work for your motherboard.\
Or try to use similar RAM to that which is already installed (type (DDRX), speed and maybe even brand wise).

## ASRock Motherboard ASRock D1800M

Vendor: mindfactory.de\
Rating: 7/10

CPU: Intel(R) Celeron(R) CPU  J1800  @ 2.41GHz\
cpu cores	: 2

IO-Panel shield for the case is part of the delivery.\
Small manual (about 15 pages) is part of the delivery.\
Two SATA-cables are part of the delivery.

There is an additional note in the package that says:\
**If only one DIMM module is installed, please install it into DDR A1.**

The heatsink is already installed on the CPU and it looks like a CPU fan **can not** be mounted.

The IO-Panel shield can just be pushed into the tower (pc case) by hand.\
(and the old one can be pushed out by hand, too)

There is **no** "EPS"-Socket for the EPS-cable (a cable from the PSU called "CPU") to get power from the PSU to the cpu...\
According to this guy https://www.youtube.com/watch?v=PXaLc9AYIcg every motherboard should have this.

#### booting into Arch Linux

Works and the cpu heatsink does not get hot at all when running an Arch Linux CLI.\
(maybe 20-30° C).


***
#### installing xfce
```
pacman -S xorg-server
```
```
pacman -S xfce4 xfce4-goodies
```
I install all packages (lateron you may leave out packages you dont want)

```
pacman -S lightdm lightdm-gtk-greeter
```

```
systemctl enable lightdm
```

```
reboot
```

seems to work

in XFCE you need to change the keyboard layout in the settings manager which is under applications\
you have to add german and remove the english (us) from the list

cpu heatsink is still not hot at all

=> everything works and the cpu heatsink will not get too hot, but the computer is relatively slow even with 8GB DDR3-1600.\
Slower than a cheap acer laptop from 2018.
***

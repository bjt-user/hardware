## acer aspire 5710 series

(10€ buy from ebay)

This laptop has a hooking mechanism that locks the screen to the laptop.\
Usually you can release that hook by pulling a plastic piece to the side, but this one is broken.

I removed those display hooks with pliers.\
It still closes ok, not perfect, but it is alright.\
Those hooks are just annoying and not worth trying to fix.

I put in a hard drive with manjaro on it and it instantly boots into Manjaro.\
Connect to wifi, install neofetch.\
The laptop has 2GB of RAM.\
CPU: Intel Core 2 T5500 (2) @ 1.667 GHz\
GPU: Intel Mobile 945GM/GMS/GME, 943/940GML Express\
DE: Xfce 4.16\
WM: Xfwm4

TODO: build a dedicated charger for this laptop.\
I have the right connectors and I have 19V psus, so I can just solder the connector to a 19V psu.

#### installing arch linux

There are two ways I could install `Arch Linux` on this laptop.\
https://wiki.archlinux.org/title/Install_Arch_Linux_from_existing_Linux \
or use the classic USB stick method \
https://wiki.archlinux.org/title/USB_flash_installation_medium

I think I will start with installation from USB stick.

#### bios

repeatingly hitting `F2` leads into BIOS.

#### keyboard comparison

| name | brand | rating |
|---|---|---|
| Cherry Stream TKL | Cherry | 5/10 |
| Logitech K835 TKL | Logitech | 5/10 |

#### keyboard layout comparison

Reachability for the special characters which are used often in C programming, bash, or other languages.

| char | reachability us layout | reachability de layout | points us | points de |
|------|------------------------|------------------------|-----------|-----------|
| `#` | ok | good | 1 | 2 |
| `\|` | ok | bad | 2 | 2 |
| \` | good | bad | 4 | 2 |
| `\` | good | bad | 6 | 2 |
| `[]` | good | bad | 8 | 2 |
| `{}` | ok | bad | 9 | 2 |
| `;` | good | ok | 11 | 3 |
| `/` | good | ok | 13 | 4 |
| `"` | ok | ok | 14 | 5 |
| `'` | good | ok | 16 | 6 |
| `,` | ok | ok | 17 | 7 |
| `<>` | ok | good | 18 | 9 |
| `_` | ok | ok | 19 | 10 |
| `-` | ok | good | 20 | 12 |
| `!` | ok | ok | 21 | 13 |
| `()` | ok | bad | 22 | 13 |
| `*` | ok | bad | 23 | 13 |
| `&` | ok | ok | 24 | 14 |
| `+` | ok | good | 25 | 16 |
| `-` | ok | good | 26 | 18 |
| `=` | ok | ok | 27 | 19 |
| `$` | ok | ok | 28 | 20 |
| `%` | ok | ok | 29 | 21 |
| `~` | bad | bad | 30 | 22 |
| `:` | good | ok | 32 | 23 |
| `.` | ok | ok | 33 | 24 |

The enter key is bigger for the `de` keyboards, which is nice, but that would be only one more point for `de`.

US layout wins.

## keycaps

#### changing keycaps on membrane keyboard

You can remove keycaps with a flat screwdriver and pop them out./
Then push it on with some force again.

#### 3d printing keycaps
try this:\
https://github.com/obra/cherry-mx-keycaps/tree/master

#### rubber o-rings for mechanical keyboards

I put rubber o-rings on a Logitech K835 to make it more quiet.\
And it really makes the keyboard more quiet, but you have to use more force to press the keys.\
=> the typing is more tiring that way, so I got rid of those o-rings

#### removing the default labeling on keycaps

Does not work with isopropyl alcohol.

With 100 grain sandpaper and a small bowl of soapy water I got rid of the prints on the keycap\
and it was pretty smooth afterwards.\
TODO: try to paint it (sand it before and maybe use primer)

## keyboard reviews

#### Logitech K385

Mechanical keyboard with aluminum case.

Cons: Very loud even with red switches.

Pros:  You can take off the keycaps easily.

#### Cherry Stream TKL

Membrane keyboard with "Cherry SX" switches.

Pros: Very quiet. Keys don't rattle, good quality.

Cons: Keycaps are hard to take off, you can pull out the underlying switch by removing the keycaps.\
And it is hard to put everything together again.\
After sanding the windows logo off the super key is lower than the other keys.\
But that might be fixable by painting it.

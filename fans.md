#### form factor

Standard PC fans are 80x80x25.

#### loudness

Replacing old fans with new Noctua ("NF-A8 PWM") fans (2200 U/min 17.7 dB(A)) did not really decrease the loudness of the fans.

Cheap Xilence 1500 rpm (80mm, 0.06A, 0.72W) seem to be a little quieter, but you can still hear them.\
But the box said 800~1800 RPM, when the internet page said 1500. (18.7dB).\
When mindfactory said 1500 U/min 15 dB(A).

#### specs to consider

- wattage
- amps
- rpm

#### noise adapters

Those are usually cables that have a resistor in them.\
I did not notice any effect with them.\
But if you put too much resistance at one of the four cables the motherboard might not boot anymore \
and even receive sustainable damage.

#### 4-pin fans

chatgpt seems to be the only source for this kind of knowledge:

1. Ground (GND): This wire is usually black and serves as the electrical ground reference for the fan. It completes the electrical circuit for the fan's operation.

1. Power (VCC or +12V): This wire is usually red and provides the fan with the necessary power supply. It typically carries a voltage of +12V to power the fan's motor.

1. Tachometer (TACH or RPM): This wire is often yellow and provides feedback from the fan to the system regarding its rotational speed. The fan generates pulses as it spins, and the frequency of these pulses corresponds to the fan's speed. The system can measure this frequency to determine the fan's RPM (rotations per minute).

1. PWM Control (PWM or Control): This wire is often blue or another color distinct from the others. It is used for controlling the fan's speed through PWM. The PWM signal is a square wave that varies in width (duty cycle). By changing the duty cycle of this signal, the fan's speed can be adjusted. A longer pulse width (higher duty cycle) results in a higher fan speed, while a shorter pulse width (lower duty cycle) slows down the fan.

## experiments

#### potentiometer between the yellow wire

=> pc doesnt boot

#### putting an obstacle between the fan blades

=> pc doesnt boot up ("fan failure")

#### TODO: fan speed controllers

try fan speed controller board with potentiometers like these:

https://www.ebay.de/itm/275747702080?hash=item4033d80540:g:BtEAAOSw1GRkEum1&amdata=enc%3AAQAIAAAA0NdzO7tCQHks1UmGAw2nxu8v518qaNEmQI4jS6M95eyFe%2FuyjvpF%2BYblAd%2FuXjOj7sAYr%2B2FPsClbSnYfZhWFneGXi1W8WD8CW5spooo1xU8BbNQqT0GQgPsEdCSMCFeQT%2FjqvoxMqWaezDlgntMyrDJuwAAgctOXMo1iR4PTjwDseKM2D3spZVYzYoC5qtWmi4nf00C4kPwxvl0OP740D7KOif0X6w3Laq%2FZRoTPzUwnwZP1wKCdRdmjynqyBcSzBqa6dz1L3NHXgWEXMGQePw%3D%7Ctkp%3ABk9SR_SY8s-4Yg

https://www.ebay.de/itm/403835853696?hash=item5e067e1b80:g:W9cAAOSwSkJi-2Fr&amdata=enc%3AAQAIAAAA0LprYN%2F%2F%2BOag09n9%2F9qCofhYl%2Bov6U8k6jBZKL1pHZ5uorkQyNDxuFnDcBSR43pxVBkZC%2Bqi2t6pyBWOyrCRyEjpgtrOh9SZ3HbRx1yM0R1b7HyhBUwsO5Vy7g11BUvl3kvcicIpZJ%2B%2BONsVeI%2Fp%2FnYh9ViBr3vEpNctarljMOdKUM4bAGxAw49qKvX8IstGCcMBjbAhSER%2BcehrkUjiAceUPcl5a7LR8mTU4i7%2BUrX0O2N8VaBM2Gs6uXo4InIHzBcPKDA4%2BLWEcsnR0KKtHIw%3D%7Ctkp%3ABk9SR_aY8s-4Yg

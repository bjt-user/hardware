https://en.wikipedia.org/wiki/Power_supply_unit_(computer)

#### modular vs. nonmodular vs. semimodular

Use **modular** power supplies!\
With cheap nonmodular power supplies you will have a lot of unused cables in your computer and cable management will be impossible.

https://en.wikipedia.org/wiki/Power_supply_unit_(computer)#Modular_power_supplies

I tried to desolder some wires from a nonmodular PSU, but that is a waste of time and is dangerous.\
Even a Weller soldering station could not transfer enough heat to solder the pins out. (very big solder pads)

There are *semi modular* psus, where the two necessary motherboard connectors are nonmodular and the rest is modular.

#### form factor

PSU standard | Width (mm) |	Height (mm) | Depth (mm) | Volume (l) |
|------------|------------|-------------|------------|------------|
ATX12V / BTX | 150 | 86 | 140 | 1.806 |
ATX large |	150 | 86 	| 180 | 2.322 |
ATX – EPS | 150 | 86 | 230 | 2.967 |
CFX12V 	| 101.6+48.4 | 86 |	96 |	0.838+0.399 |
SFX12V | 	125 | 63.5 |	100 | 0.793 |
TFX12V | 85 | 64 | 175 | 0.952 |
LFX12V 	| 62 | 72 |	210 | 0.937 |
FlexATX | 81.5 | 40.5 | 150 | 0.495 |

TFX fits my small server case.

#### ratings

| name | brand | form factor | comment | rating |
| --- | --- | --- | --- | --- |
| 300 Watt Inter-Tech Argus TFX Non-Modular | Inter-Tech | TFX | very loud | 0/10 |
| 500 Watt be quiet! System Power 9 CM Modular 80+ Bronze | be quiet! | ATX | very quiet | 6/10 |

**Manufacturer: HP**\
**Modell: Compaq 615**\
repairability score: 0/10

goal: trying to restore this old laptop from 2015

#### Disassembly

Not all screws can be completely removed.

Some screws are phillips head screws.
Others are Torx.
And those don't seem to be the same size.

This laptop is a pain to disassemble.

**If I get it dissasembled I will replace all torx screws with phillips heads.**

Even with all screws loosened, the laptop lid cannot easily be removed.\
I try to do it with a screwdriver and my 3d printed opening tool.\
=> no success, it is definatelly harder than with most laptops.

I had to rip the wifi cables out to get a second cover over the electronics off...\
Those two wifi cables were connected to the monitor.\
Probably to get a better rf ground and some sort of antenna.\
I could have disconnected those from the other side as there is an opening...

I had to use a lot of force to get the mainboard off.

Another plastic thing broke.\
I glued a part of the fan back together.

I soldered the wifi chip antenna and rf ground cables back to their plugs.

I took out the bluetooth chip because it was hanging loose in there and I don't know where to put it.\
I don't use bluetooth anyway so I take it out.

After putting it all together some hooks from the plastic don't go back together.\
The problem was that I did not have the right tools at the beginning.\
Maybe I destroyed some plastic hooks.\
Maybe I also need to get some additional laptop opening tools.\
(there are some interesting tools in the market)\
But after screwing every screw back in or better said replacing torx screws with my own\
everything holds together again.

Some cables at the battery are not positioned correctly.\
It is booting but shows a hardware error: cpu error.\
But it still boots into the operating system.\
The fan is very loud. (probably related to the cpu error)\
Wifi connection seems to have a loose contact.\
The lid is not closing perfectly anymore (I must have made a assembly error).

Replaced the AMD Athlon CPU with an AMD Atholn II CPU from another laptop.\
But there seem to be only 2 CPUs that are compatible with this board:\
https://support.hp.com/gb-en/document/c01768616

To fasten the SSD you need a M2.5*10 screw.

It does not boot with the new CPU.\
I think the ROM works but the CPU doesn't work with this board.\
There are also signs of water damage inside the laptop.\
I'm not sure if this laptop is fixable.

You can get a hp compaq motherboard at aliexpress.com.\
Compare the picture with your broken motherboard before trying that.

*Waiting for aliexpress mobo.*
Motherboard with CPU (but without heat sink, RAM and some other small stuff) arrived.

Cleaned off dry thermal paste from delivered motherboard (which is not new, but the company checked it for functionality).\
Applied thermal paste that was delivered with the board and screwed heat sink and other stuff from old mobo onto it.\
(there is still some thermal paste between pins I wonder if that will cause a short)

Installed new motherboard.\
It turns on but no screen.\
Trying to get into BIOS with F2 and F10.\
=> did not work

**Not worth putting more time and money into this laptop.**

Lessongs learned:
- chinese replacement motherboards don't seem to work
- this laptop is not very repairable
